from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.http import require_POST

# Create your views here.

def index(request):
    return render(request, 'index.html')

@login_required
def index_logado(request):
    return render(request, 'index_logado.html')

def efetuar_login(request):
    return render(request, 'login.html')

@login_required
def efetuar_logout(request):
    logout(request)
    return redirect('index')

def register(request):
    return render(request, 'registrar.html')

@require_POST
def cadastrar(request):
    username = request.POST['username']
    senha = request.POST['password']

    novoUsuario = User.objects.create_user(username=username, email=username, password=senha)
    novoUsuario.save()

    return redirect('login')

@require_POST
def entrar(request):
    username = request.POST['username']
    senha = request.POST['password']
    usuario_aux = User.objects.get(email=username)
    usuario = authenticate(username=usuario_aux.username,
                           password=senha)
    if usuario is not None:
        login(request, usuario)
        return redirect('index_logado')

    return redirect('index')