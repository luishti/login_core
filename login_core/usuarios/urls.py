from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('cadastrar', views.cadastrar, name='cadastrar'),
    path('entrar', views.entrar, name='entrar'),
    path('efetuar_logout', views.efetuar_logout, name='efetuar_logout'),
    path('efetuar_login', views.efetuar_login, name='efetuar_login'),
    path('register', views.register, name='register'),
    path('index_logado', views.index_logado, name='index_logado'),
]
